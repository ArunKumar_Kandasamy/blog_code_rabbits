Rails.application.config.before_initialize do
       $theme_background_color = "#ffffff"
       $theme_hightlight_color = "#afafae"
       $theme_color = "#000"
       $theme_font_color = "#666666" 
       $theme_header_color = "#afafae"
       $base_url = "http://www.challengesoap.com"
       $host = "www.challengesoap.com"
       $postmark_username = "3ab0042b-1f38-4bd3-b5d2-7754ed00ca15"
       $postmark_mailer = "info@coderabbits.com"
       $secret_key_base =  "1370ca9d8aec2f217d2c6f6bbd7650ebeec0eb2cac4a460ffebefc5f265a950a26f802f69fe5280ddda9b71ac45ca4a4acad90a329d5b1e2ec582c12512a4675"
       $generic_header = "FALSE"
       $generic_header_name = "challengesoap_blog_sm"
       $restriction_email_id = /\A([\w\.%\+\-]+)@coderabbits.com\z/i
       $disqus_shortname = "challengesoapcom"
end
