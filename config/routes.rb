Rails.application.routes.draw do


  resources :subscribers

  mount Ckeditor::Engine => '/ckeditor'
  constraints :subdomain => /^admin/ do
    resources :settings
    resources :articles
    get 'tags/:tag', to: 'articles#index', as: :tags, constraints: {subdomain: 'admin'}
    get 'check_image_type_exist' =>"image_types#check_image_type_exist", constraints: {subdomain: 'admin'}
    get 'check_image_name_exist' =>"image_types#check_image_name_exist", constraints: {subdomain: 'admin'}
    resources :cms_content_types do
      resources :cms_contents do
          collection { post :sort }
      end
    end
  end

  constraints :subdomain => /^blog/ do
    root 'posts#index'
    get 'tags/:tag', to: 'posts#index', as: :tag, constraints: {subdomain: 'blog'}
    get 'archives/:year/:month', to: 'posts#index', as: :archives, constraints: {subdomain: 'blog'}
    resources :posts
  end

  devise_for :users
  devise_scope :user do
    get 'register', to: 'devise/registrations#new', as: :register, constraints: {subdomain: 'admin'}
    get 'login', to: 'devise/sessions#new', as: :login, constraints: {subdomain: 'admin'}
  end
  get '', to: 'articles#dashboard', as: :dashboard, constraints: {subdomain: 'admin'}
  get '', to: 'posts#index', constraints: {subdomain: 'blog'}

  resources :users

  match "*path", to: "posts#catch_404", via: :all

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end