require 'test_helper'

class CmsContentsControllerTest < ActionController::TestCase
  setup do
    @cms_content = cms_contents(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cms_contents)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cms_content" do
    assert_difference('CmsContent.count') do
      post :create, cms_content: {  }
    end

    assert_redirected_to cms_content_path(assigns(:cms_content))
  end

  test "should show cms_content" do
    get :show, id: @cms_content
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cms_content
    assert_response :success
  end

  test "should update cms_content" do
    patch :update, id: @cms_content, cms_content: {  }
    assert_redirected_to cms_content_path(assigns(:cms_content))
  end

  test "should destroy cms_content" do
    assert_difference('CmsContent.count', -1) do
      delete :destroy, id: @cms_content
    end

    assert_redirected_to cms_contents_path
  end
end
