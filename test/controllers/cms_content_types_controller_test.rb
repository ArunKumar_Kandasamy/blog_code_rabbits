require 'test_helper'

class CmsContentTypesControllerTest < ActionController::TestCase
  setup do
    @cms_content_type = cms_content_types(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:cms_content_types)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create cms_content_type" do
    assert_difference('CmsContentType.count') do
      post :create, cms_content_type: {  }
    end

    assert_redirected_to cms_content_type_path(assigns(:cms_content_type))
  end

  test "should show cms_content_type" do
    get :show, id: @cms_content_type
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @cms_content_type
    assert_response :success
  end

  test "should update cms_content_type" do
    patch :update, id: @cms_content_type, cms_content_type: {  }
    assert_redirected_to cms_content_type_path(assigns(:cms_content_type))
  end

  test "should destroy cms_content_type" do
    assert_difference('CmsContentType.count', -1) do
      delete :destroy, id: @cms_content_type
    end

    assert_redirected_to cms_content_types_path
  end
end
