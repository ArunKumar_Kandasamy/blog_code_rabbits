class CmsContentType < ActiveRecord::Base
    #GEMS USED
  #ACCESSORS
  #ASSOCIATIONS
  has_many :cms_contents, class_name:"CmsContent"
  #VALIDATIONS
  validates :name, :presence => true, :uniqueness => true
   validates :image_type, :presence => true, :uniqueness => true
  validates :width, :presence => true
  validates :height, :presence => true
  #CALLBACKS
  before_save :downcase_fields
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS
  #JOBS
  #PRIVATE 
  def downcase_fields
    self.name.downcase!
    self.image_type.downcase!
  end
end
