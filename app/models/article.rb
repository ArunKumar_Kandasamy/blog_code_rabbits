class Article < ActiveRecord::Base
  belongs_to :user
  acts_as_taggable
  extend FriendlyId
  friendly_id :title, use:  [:slugged, :history]
   scope :published, -> { where(is_published: true) }
   scope :recent, -> { order("created_at DESC") }
   validates_presence_of :title, :post, :tag_list

   def self.search(search)
      if search
        where("post iLIKE ? OR title iLIKE ?", "%#{search}%" , "%#{search}%")
      else
        all
      end
    end
end
