class CmsContent < ActiveRecord::Base
    #GEMS USED
  #ACCESSORS
  attr_accessor :crop_x, :crop_y, :crop_w, :crop_h, :processing, :deferred_image
  attr_writer :deferred_image
  #ASSOCIATIONS
  has_attached_file :avatar,

  styles: lambda { |a| a.instance.is_image? ? {:large => a.instance.styles,:medium => "300x300!", :thumb => "100x100!"}  : {:thumb => { :geometry => "100x100#", :format => 'jpg', :time => 10}, :medium => { :geometry => "300x300#", :format => 'jpg', :time => 10}}},
  processors: lambda { |a| a.is_video? ? "" : [ :cropper ] }
  belongs_to :cms_content_type, class_name:"CmsContentType"
  #VALIDATIONS
  validates :title, :presence => true
  validates_attachment_content_type :avatar, :content_type => ['application/x-shockwave-flash', 'application/x-shockwave-flash', 'application/flv', 'video/mp4','image/jpeg', 'image/jpg', 'image/png']
  #CALLBACKS
  after_update :reprocess_avatar, :if => :cropping?
  after_create :update_sort_order
  after_save :assign_deferred_image
  before_save :link_validation
  #SCOPES
  #CUSTOM SCOPES
  #OTHER METHODS

  def update_sort_order
     @sort_maximum= CmsContent.maximum("sort_order") ? CmsContent.maximum("sort_order") : 0
      @sort_maximum+=1
      self.sort_order=@sort_maximum
      self.save
  end

  def validate_image_size
    if avatar.file? && avatar.size > get_current_file_size_limit
      errors.add_to_base(" ... Your error message")
    end
  end 

  def styles
    if self.cms_content_type.present?
      return "#{self.cms_content_type.width}x#{self.cms_content_type.height}!"
    else
      return "1000x1000!"
    end
  end

  def is_image?
    [ 'image/jpeg', 
      'image/jpg',
      'image/png' ].include?(avatar.content_type)
  end

  def is_video?
    [ 'application/x-mp4',
      'video/mp4',
      'video/mpeg',
      'video/quicktime',
      'video/x-la-asf',
      'video/x-ms-asf',
      'video/x-msvideo',
      'video/x-sgi-movie',
      'video/x-flv',
      'flv-application/octet-stream',
      'video/3gpp',
      'video/3gpp2',
      'video/3gpp-tt',
      'video/BMPEG',
      'video/BT656',
      'video/CelB',
      'video/DV',
      'video/H261',
      'video/H263',
      'video/H263-1998',
      'video/H263-2000',
      'video/H264',
      'video/JPEG',
      'video/MJ2',
      'video/MP1S',
      'video/MP2P',
      'video/MP2T',
      'video/mp4',
      'video/MP4V-ES',
      'video/MPV',
      'video/mpeg4',
      'video/mpeg4-generic',
      'video/nv',
      'video/parityfec',
      'video/pointer',
      'video/raw',
      'video/rtx' ].include?(avatar.content_type)
  end

  def cropping?
    !crop_x.blank? && !crop_y.blank? && !crop_w.blank? && !crop_h.blank?
  end


  def avatar_geometry(style = :original)
    @geometry ||= {}
    @geometry[style] ||= Paperclip::Geometry.from_file(avatar.path(style))
  end

  #JOBS
  #PRIVATE 

  def reprocess_avatar
    return unless (cropping? && !processing)
    self.processing = true
    avatar.reprocess!
    self.processing = false
  end

  def assign_deferred_image
    if @deferred_image
      self.avatar = @deferred_image
      @deferred_image = nil
      save!
    end
  end
  
  def link_validation
    if self.link.match("https://").present?
    elsif self.link.match("http://").present?
    else
      self.link = "http://" + self.link
    end
  end
end
