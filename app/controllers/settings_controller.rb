class SettingsController < ApplicationController
  before_action :set_setting, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @settings = Setting.all
    respond_with(@settings)
  end

  def show
    respond_with(@setting)
  end

  def new
     if Setting.first.present?
         redirect_to settings_path
     else
        @setting = Setting.new
        respond_with(@setting)
    end
  end

  def edit
  end

  def create
       unless Setting.first.present?
          @setting = Setting.new(setting_params)
          @setting.save
       end
        redirect_to settings_path
  end

  def update
    @setting.update(setting_params)
    @settings = Setting.all
    redirect_to settings_path
  end

  def destroy
    @setting.destroy
    respond_with(@setting)
  end

  private
    def set_setting
      @setting = Setting.find(params[:id])
    end

    def setting_params
      params.require(:setting).permit(:company_name, :fb_link, :tw_link, :gp_link, :pt_link, :avatar,:favicon)
    end
end
