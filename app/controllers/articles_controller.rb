class ArticlesController < ApplicationController
  before_filter :authenticate_user!
  before_action :set_article, only: [:show, :edit, :update, :destroy]
  respond_to :html

  def dashboard
  end

  def index
    if params[:tag]
      @articles = Article.tagged_with(params[:tag])
    else
      @articles = Article.all
    end
    respond_with(@articles)
  end

  def show
    respond_with(@article)
  end

  def new
    @article = Article.new
    respond_with(@article)
  end

  def edit

  end

  def create
    @article = current_user.articles.build(article_params)
    @article.save
    respond_with(@article)
  end

  def update
    @article.update(article_params)
    $fetch_theme_from_db = Article.first.title
    respond_with(@article)
  end

  def destroy
    @article.destroy
    respond_with(@article)
  end

  private
    def set_article
      @article = Article.find_by_slug(params[:id])
    end

    def article_params
      params.require(:article).permit(:user_id, :title, :post, :is_published,:tag_list,:meta_description)
    end
end
