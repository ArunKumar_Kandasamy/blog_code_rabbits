class CmsContentsController < ApplicationController
  before_action :set_cms_content, only: [:show, :edit, :update, :destroy]
  before_action :set_cms_content_type

  respond_to :html

  def index
    @cms_contents = CmsContent.all
    respond_with(@cms_contents)
  end

  def show
    respond_with(@cms_content)
  end

  def new
    @cms_content = CmsContent.new
    respond_with(@cms_content)
  end

  def edit
    render :template => "cms_contents/edit"
  end

  def create
     cms_content_type = @cms_content_type
    @cms_content = @cms_content_type.cms_contents.build(cms_content_params)
    if @cms_content.save
      if params[:cms_content][:avatar].blank?
        flash[:notice] = "Cms content was successfully created."
        redirect_to @cms_content
      else
        render :action => "crop"
      end
    else
      render :template => "cms_contents/new"
    end

  end


  def sort
    params[:cms_content].each_with_index do |id, index|
      CmsContent.where(id: id).update_all(sort_order: index+1)
    end
    render nothing: true
  end

  def update

    if @cms_content.update(cms_content_params)

      if params[:cms_content][:avatar].blank?
        if params[:cms_content][:crop_x].present?
          flash[:notice] = "Cms content was successfully updated."
          redirect_to @cms_content_type
        else
          render :action => "crop"
        end
      else
        render :action => "crop"
      end
    else
      format.html { render :edit }
      format.json { render json: @cms_content.errors, status: :unprocessable_entity }
    end

  end


  def destroy
    @cms_content.destroy
    redirect_to @cms_content_type
  end

  private

    def set_cms_content_type
      @cms_content_type = CmsContentType.find(params[:cms_content_type_id])
    end

    def set_cms_content
      @cms_content = CmsContent.find(params[:id])
    end 

    def cms_content_params
      params.require(:cms_content).permit(:title, :link,:avatar,:description, :image_type_id, :category_id, :alt_text, :crop_x, :crop_y, :crop_w, :crop_h)
    end
end
