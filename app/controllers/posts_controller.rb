class PostsController < ApplicationController
  before_action :set_post, only: [:show]
  before_action :set_setting

  respond_to :html
  # GET /posts
  # GET /posts.json
  def index
    @subscriber = Subscriber.new
    if params[:tag]
      @posts = Article.published.tagged_with(params[:tag])
    elsif params[:month]
      @posts = Article.published.where("extract(year  from created_at) = ? AND extract(month  from created_at) = ? ", params[:year], params[:month]).order("created_at DESC")
    else
      @posts = Article.published.search(params[:search]).order("created_at")
    end
    @posts = @posts.reverse
    @post_months = Article.published.group_by { |t| t.created_at.beginning_of_month }
    @recent_posts = Article.published.recent
    respond_with(@posts)
  end

  # GET /posts/1
  # GET /posts/1.json
  def show
    @subscriber = Subscriber.new
    @post_months = Article.published.group_by { |t| t.created_at.beginning_of_month }
    @recent_posts = Article.published.recent
    if request.path != post_path(@post)
      redirect_to @post, status: :moved_permanently
    end
  end


    def catch_404
      raise ActionController::RoutingError.new(params[:path])
    end

  
  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Article.find_by_slug(params[:id])
    end

    def set_setting
       @setting = Setting.first
    end
  
end
