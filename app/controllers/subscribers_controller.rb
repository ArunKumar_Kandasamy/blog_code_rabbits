class SubscribersController < ApplicationController
  before_filter :authenticate_user!, except: [:create ]
  before_action :set_subscriber, only: [:destroy,:update]

  respond_to :html

  def index
    @subscribers = Subscriber.all
    respond_with(@subscribers)
  end

  def create
    @subscriber = Subscriber.new(subscriber_params)
    @subscriber.save
    flash[:notice] = "Thanks! we will send you updates about the  latest blogs"
    redirect_to root_path
  end

  def update
    @subscriber.update(subscriber_params)
    respond_with(@subscriber)
  end

  def destroy
    @subscriber.destroy
    respond_with(@subscriber)
  end

  private
    def set_subscriber
      @subscriber = Subscriber.find(params[:id])
    end

    def subscriber_params
      params.require(:subscriber).permit(:email, :email_count)
    end
end