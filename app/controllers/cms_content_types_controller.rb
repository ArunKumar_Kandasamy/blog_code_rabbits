class CmsContentTypesController < ApplicationController
  before_action :set_cms_content_type, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @cms_content_types = CmsContentType.all
    respond_with(@cms_content_types)
  end

  def show
    respond_with(@cms_content_type)
  end

  def new
    @cms_content_type = CmsContentType.new
    respond_with(@cms_content_type)
  end

  def edit
  end



  def check_image_type_exist
    image_type = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?
      @cms_content_type = CmsContentType.where("lower(name) = ?", image_type.downcase).where("id != ?", validate_ajax_except).first
    else
      @cms_content_type = CmsContentType.where("lower(name) = ?", image_type.downcase).first
    end
    if @cms_content_type.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  def check_image_name_exist
    image_type = params[:fieldValue]
    validate_ajax_except = params[:validate_ajax_except]
    if validate_ajax_except.present?  
      @cms_content_type = CmsContentType.where("lower(image_type) = ?", image_type.downcase).where("id != ?", validate_ajax_except).first
    else
      @cms_content_type = CmsContentType.where("lower(image_type) = ?", image_type.downcase).first
    end
    if @cms_content_type.present?
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => false } }
      end
    else
      respond_to do |format|
        format.json  { render :json =>{ :brand_exist => true } }
      end
    end
  end

  def create
    @cms_content_type = CmsContentType.new(cms_content_type_params)
    @cms_content_type.save
    respond_with(@cms_content_type)
  end

  def update
    @cms_content_type.update(cms_content_type_params)
    respond_with(@cms_content_type)
  end

  def destroy
    @cms_content_type.destroy
    respond_with(@cms_content_type)
  end

  private
    def set_cms_content_type
      @cms_content_type = CmsContentType.find(params[:id])
    end

    def cms_content_type_params
      params.require(:cms_content_type).permit(:name, :image_type, :height, :width, :is_video)
    end
end
