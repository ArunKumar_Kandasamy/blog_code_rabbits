class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

   before_filter :configure_devise_params, if: :devise_controller?

   def after_inactive_sign_up_path_for(resource)
     flash[:notice] = 'A message with a confirmation link has been sent to your email address. Please follow the link to activate your account.'
     new_user_session_path
  end


  def configure_devise_params
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :designation, :email, :password, :password_confirmation)
    end
  end                  


    rescue_from ActiveRecord::RecordNotFound do
      flash[:notice] = 'The page you tried to access does not exist'
      redirect_to root_path
    end

    rescue_from ActionController::RoutingError, :with => :error_render_method

    def error_render_method
      flash[:notice] = 'The page you tried to access does not exist'
       redirect_to root_path
    end 
end
