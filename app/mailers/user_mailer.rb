class UserMailer < ActionMailer::Base
  include Devise::Mailers::Helpers

  default from: "preethi.m@coderabbits.com"

  def message
    mail(
      :subject => 'Hello from Postmark',
      :to  => 'preethi.m@coderabbits.com',
      :from => 'support@coderabbits.com',
      :html_body => '<strong>Hello</strong> dear Postmark user.',
      :track_opens => 'true')
  end

  def confirmation_instructions(record)
    devise_mail(record, :confirmation_instructions)
  end

  def reset_password_instructions(record)
    devise_mail(record, :reset_password_instructions)
  end

  def unlock_instructions(record)
    devise_mail(record, :unlock_instructions)
  end

  # you can then put any of your own methods here
end