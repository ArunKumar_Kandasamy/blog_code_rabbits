module ApplicationHelper

  def parse_shortcode(model)
    model.post.gsub(/\<img.*src\=['"](.*)['"].*.*style\=['"](.*)['"].*\/\>/) do |url|
     "#{pin_it_button media: image_url($1),
                  description: @post.title} #{url.gsub(/\<img/, '<img class="img-responsive"')}"
    end
  end

  def find_header_type(partial)
      if  $generic_header == "FALSE"
            if  lookup_context.find_all("posts/#{ $generic_header_name }/_#{partial}").any? 
                 return "posts/#{ $generic_header_name }/#{partial}" 
             else
                   return "posts/#{partial}" 
            end
        else 
                return "posts/#{partial}" 
          end 
  end


end
