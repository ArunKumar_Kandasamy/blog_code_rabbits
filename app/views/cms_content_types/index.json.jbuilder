json.array!(@cms_content_types) do |cms_content_type|
  json.extract! cms_content_type, :id
  json.url cms_content_type_url(cms_content_type, format: :json)
end
