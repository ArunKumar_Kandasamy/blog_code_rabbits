json.array!(@cms_contents) do |cms_content|
  json.extract! cms_content, :id
  json.url cms_content_url(cms_content, format: :json)
end
