class AddFaviconToSettings < ActiveRecord::Migration
  def change
       change_table :settings do |t|
          t.attachment :favicon
        end
  end
end
