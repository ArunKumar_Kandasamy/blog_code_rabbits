class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.integer :user_id
      t.string :title
      t.text :post
      t.boolean :is_published

      t.timestamps
    end
  end
end
