class CreateSettings < ActiveRecord::Migration
  def change
    create_table :settings do |t|
      t.attachment :avatar
      t.string :company_name
      t.string :fb_link
      t.string :tw_link
      t.string :gp_link
      t.string :pt_link
      t.timestamps
    end
  end
end
