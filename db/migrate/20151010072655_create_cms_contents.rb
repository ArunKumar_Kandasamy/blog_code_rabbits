class CreateCmsContents < ActiveRecord::Migration
  def change
    create_table :cms_contents do |t|
      t.attachment :avatar
      t.integer :cms_content_type_id
      t.string :alt_text
      t.integer :sort_order
      t.string :link
      t.text :description
      t.string  :title

      t.timestamps
    end
  end
end
